from Hervivoro import Hervivoro
from Carnivoro import Carnivoro


class Omnivoro(Carnivoro, Hervivoro):
    def __init__(self, nombre: str, especie: str, tamagno: float, habitad: str, plantasPreferidas: str, dieta: str):
        Carnivoro.__init__(self, nombre, especie, tamagno, habitad)
        Hervivoro.__init__(self, nombre, especie, tamagno, plantasPreferidas)
        self.dieta = dieta

    def __str__(self):
        return f'Animal: [nombre:{self.nombre}, especie: {self.especie}, tamaño: {self.tamagno} m.], Omnivoro: [plantas preferidas: {self.plantasPreferidas}, habitad: {self.habitad}, dieta: {self.dieta}]'


omnivoro = Omnivoro("oso","Ursus arctos", 2, "bosque", "frutas y carne", "variada")
print(omnivoro)
