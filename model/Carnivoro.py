class Carnivoro:
    def __init__(self,nombre : str, especie: str, tamagno: float, habitad: str):
        self.nombre = nombre
        self.especie = especie
        self.tamagno = tamagno
        self.habitad = habitad

    def __str__(self):
        return f'Carnivoro : [nombre: {self.nombre}, especie: {self.especie}, tamaño: {self.tamagno} m., habitad: {self.habitad}]'
    

leon = Carnivoro("Leon","Panthera leo",1.9,"planicies de la sabana")
print(leon)