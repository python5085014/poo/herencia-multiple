class Hervivoro:
    def __init__(self, nombre : str, especie: str, tamagno: float, plantasPreferidas: str):
        self.nombre = nombre
        self.especie = especie
        self.tamagno = tamagno
        self.plantasPreferidas = plantasPreferidas

    def __str__(self):
        return f'Hervivoro : [nombre: {self.nombre}, especie: {self.especie}, tamaño: {self.tamagno} m., plantas preferidas: {self.plantasPreferidas}]'
    


gorila = Hervivoro("Gorila","Gorilla gorilla",1.7,"tallos y frutas")
print(gorila)